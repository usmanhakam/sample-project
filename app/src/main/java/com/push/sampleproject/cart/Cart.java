package com.push.sampleproject.cart;

import android.content.SharedPreferences;

import com.barfee.mart.exception.ProductNotFoundException;
import com.barfee.mart.exception.QuantityOutOfRangeException;
import com.barfee.mart.interfaces.CartObserver;
import com.barfee.mart.models.Product;
import com.barfee.mart.utils.Globals;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import io.intercom.android.sdk.Intercom;

/**
 * A representation of shopping cart.
 * <p/>
 * A shopping cart has a map of {@link Saleable} products to their corresponding quantities.
 */
public class Cart implements Serializable {
    private static final long serialVersionUID = 42L;
    private static final String PREFS_NAME = "cartitems";
    private LinkedHashMap<Integer, Saleable> cartItemMap = new LinkedHashMap<>();

    private List<CartObserver> observers = new ArrayList<CartObserver>();

    SharedPreferences.Editor editor = Globals.getUsage().mContext.getSharedPreferences(PREFS_NAME, 0).edit();
    SharedPreferences prefs = Globals.getUsage().mContext.getSharedPreferences(PREFS_NAME, 0);

    /**
     * Add a quantity of a certain {@link Saleable} product to this shopping cart
     *
     * @param sellable the product will be added to this shopping cart
     * @param quantity the new quantity of product
     */
    public void add(Saleable sellable, int id, int quantity) {
//        Bundle bundle = new Bundle();
//        bundle.putString("phone", Globals.getUsage().appSession.getPhone());

        if (cartItemMap.containsKey(id)) {
            Product product = (Product) cartItemMap.get(id);
            product.setQuantity(quantity);
            cartItemMap.put(id, product);

//            bundle.putString("name", product.getName());


//            FirebaseAnalytics.getInstance(Globals.getUsage().mContext).logEvent("cart_add", bundle);
        } else {
            cartItemMap.put(id, sellable);

//            bundle.putString("name", sellable.getName());
//            FirebaseAnalytics.getInstance(Globals.getUsage().mContext).logEvent("cart_item", bundle);

        }

        Map<String, Object> userMap = new HashMap<>();
        Map<String, Object> customAttributes = new HashMap<>();
        customAttributes.put("order_abondon",1);
        userMap.put("custom_attributes", customAttributes);
        Intercom.client().updateUser(userMap);

        saveDeleteItem("" + id, (Product) sellable, true);
        notifyAllObservers(false);

    }


    /**
     * Set new quantity for a {@link Saleable} product in this shopping cart
     *
     * @param quantity the new quantity will be assigned for the product
     * @throws ProductNotFoundException    if the product is not found in this shopping cart.
     * @throws QuantityOutOfRangeException if the quantity is negative
     */
    public void update(int id, int quantity) throws ProductNotFoundException, QuantityOutOfRangeException {
        if (!cartItemMap.containsKey(id)) return;
        if (quantity < 0)
            throw new QuantityOutOfRangeException(quantity + " is not a valid quantity. It must be non-negative.");

        Product product = (Product) cartItemMap.get(id);
        product.setQuantity(quantity);
        cartItemMap.put(id, product);

        saveDeleteItem("" + id, product, true);

        int productQuantity = cartItemMap.get(id).getQuantity();

        notifyAllObservers(false);
    }

    /**
     * Remove a certain quantity of a {@link Saleable} product from this shopping cart
     *
     * @param id the id of product which will be removed
     * @throws ProductNotFoundException    if the product is not found in this shopping cart
     * @throws QuantityOutOfRangeException if the quantity is negative or more than the existing quantity of the product in this shopping cart
     */
    public void remove(int id) throws ProductNotFoundException, QuantityOutOfRangeException {
        if (!cartItemMap.containsKey(id)) return;

        int productId = ((Product) cartItemMap.get(id)).getId();
        if (productId == id) {
            cartItemMap.remove(id);
            saveDeleteItem(id + "", null, false);
        }


        notifyAllObservers(false);
    }

//    /**
//     * Remove a {@link Saleable} product from this shopping cart totally
//     *
//     * @param sellable the product to be removed
//     * @throws ProductNotFoundException if the product is not found in this shopping cart
//     */
//    public void remove(final Saleable sellable) throws ProductNotFoundException {
//        if (!cartItemMap.containsKey(sellable)) throw new ProductNotFoundException();
//
//        int quantity = cartItemMap.get(sellable);
//        cartItemMap.remove(sellable);
//        totalPrice = totalPrice.subtract(sellable.getPrice().multiply(BigDecimal.valueOf(quantity)));
//        totalQuantity -= quantity;
//    }

    /**
     * Remove all products from this shopping cart
     */
    public void clear() {
        editor.clear();
        editor.commit();
        cartItemMap.clear();
        notifyAllObservers(true);
    }


    /**
     * Get quantity of a {@link Saleable} product in this shopping cart
     *
     * @param id the product id
     * @return The product quantity in this shopping cart
     * @throws ProductNotFoundException if the product is not found in this shopping cart
     */
    public Product getProduct(int id) throws ProductNotFoundException {
        if (!cartItemMap.containsKey(id)) return null;
        return (Product) cartItemMap.get(id);
    }


    /**
     * Get set of products in this shopping cart
     *
     * @return Collection of {@link Saleable} products in this shopping cart
     */
    public Collection<Saleable> getProducts() {
        return cartItemMap.values();
    }

    /**
     * Get a map of products to their quantities in the shopping cart
     *
     * @return A map from product to its quantity in this shopping cart
     */
    public ArrayList<Product> getItemWithQuantity() {
        ArrayList<Product> list = new ArrayList<Product>();
        for (Entry<Integer, Saleable> entry : cartItemMap.entrySet()) {
            list.add((Product) entry.getValue());
        }
        return list;
    }

//    @Override
//    public String toString() {
//        StringBuilder strBuilder = new StringBuilder();
//        for (Entry<Integer, Saleable> entry : cartItemMap.entrySet()) {
//            strBuilder.append(String.format("Product: %s, Unit Price: %f , Quantity: %d , ID: %d%n", entry.getValue().getName(), entry.getValue().getPrice(), entry.getValue().getQuantity(), entry.getKey()));
//        }
//        return strBuilder.toString();
//    }


    public void attach(CartObserver observer) {
        observers.add(observer);
//        Log.v("observersizeattach", observers.size() + "");
    }

    public void detach(CartObserver observer) {
        observers.remove(observer);
//        Log.v("observersizedetach", observers.size() + "");
    }

    public void notifyAllObservers(boolean isCheckout) {
        for (CartObserver observer : observers) {
            observer.update(isCheckout);
        }
    }

    public Product has(Product product, int id) {
        if (!cartItemMap.containsKey(id)) {
            return product;
        } else {
            return (Product) cartItemMap.get(id);
        }
    }

    public boolean check(int id) {
        if (!cartItemMap.containsKey(id)) {
            return false;
        } else {
            return true;
        }
    }

    public JSONArray checkout() {

        try {
            JSONArray jsonArray = new JSONArray();
            for (Entry<Integer, Saleable> entry : cartItemMap.entrySet()) {
                JSONObject object = new JSONObject();
                object.put("product_id", ((Product) entry.getValue()).getId());
                object.put("quantity", ((Product) entry.getValue()).getQuantity());
                jsonArray.put(object);
            }

            return jsonArray;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public int totalQuantity() {
        int quantity = 0;
        for (Entry<Integer, Saleable> entry : cartItemMap.entrySet()) {
            quantity = quantity + entry.getValue().getQuantity();
        }
        return quantity;
    }

    public double totalPrice() {
        double price = 0;
        for (Entry<Integer, Saleable> entry : cartItemMap.entrySet()) {
            price = price + (entry.getValue().getQuantity() * entry.getValue().getPrice());
        }
        return price;
    }

    public void loadCart() {

        if (editor != null) {
            for (Entry entry : prefs.getAll().entrySet())
                try {
                    cartItemMap.put(Integer.parseInt((String) entry.getKey()), new Product(new JSONObject((String) entry.getValue())));
                    notifyAllObservers(false);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
        }

    }

    private void saveDeleteItem(String id, Product product, boolean isAdded) {
        if (isAdded)
            editor.putString(id, product.getJson());
        else
            editor.remove(id);
        editor.commit();
    }
}
