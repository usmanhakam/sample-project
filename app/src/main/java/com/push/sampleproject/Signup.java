package com.push.sampleproject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.barfee.mart.interfaces.ResponseListenerForApiCall;
import com.barfee.mart.models.Customer;
import com.barfee.mart.utils.AlertDialogs;
import com.barfee.mart.utils.Constants;
import com.barfee.mart.utils.Globals;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Signup extends AppCompatActivity {

    @Bind(R.id.et_phone)
    EditText editTextPhone;
    @Bind(R.id.et_password)
    EditText editTextPassword;
    @Bind(R.id.et_address)
    EditText editTextAddress;
    @Bind(R.id.et_address_2)
    EditText editTextAddress2;
    @Bind(R.id.et_username)
    EditText editTextUsername;
    @Bind(R.id.til_phone)
    TextInputLayout textInputLayoutPhone;
    @Bind(R.id.til_password)
    TextInputLayout textInputLayoutPassword;
    @Bind(R.id.til_address)
    TextInputLayout textInputLayoutAddress;
    @Bind(R.id.til_address_2)
    TextInputLayout textInputLayoutAddress2;
    @Bind(R.id.til_username)
    TextInputLayout textInputLayoutUsername;

    @Bind(R.id.btn_signup)
    Button buttonSignup;
    @Bind(R.id.tv_code)
    TextView textViewCode;
    @Bind(R.id.tv_signup_heading)
    TextView textViewHeading;

    private Globals mGlobals;
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        MyApplication.getInstance().facebookEventTracking("On SignUp");
        initActivity();
        initViews();
    }

    public String addressFromLocation = "";
    public double lati = 0;
    public double longi = 0;

    @Override
    protected void onResume() {
        super.onResume();
        mGlobals = Globals.getInstance(this);

        if (!mGlobals.addressName.equals("")) {
            addressFromLocation = mGlobals.addressName;
            editTextAddress.setText(addressFromLocation);
            mGlobals.addressName = "";
        }
        if (mGlobals.lati != 0) {
            this.lati = mGlobals.lati;
            mGlobals.lati = 0;
        }
        if (mGlobals.longi != 0) {
            this.longi = mGlobals.longi;
            mGlobals.longi = 0;
        }
        if (mGlobals.appSession.hasVendor()) {
            addressFromLocation = mGlobals.appSession.getAddress();
            editTextAddress.setText(addressFromLocation);
        }
    }

    @OnClick(R.id.tv_signup_heading)
    public void onBack(View v) {

        finish();
    }

    @OnClick(R.id.et_address)
    public void onAddressClick(View v) {
        mGlobals.fromWhere = Constants.SIGN_UP;
        startActivity(new Intent(mGlobals.mContext, DeliveryLocation.class));
    }

    private void initViews() {

        mGlobals.setSelector(this, buttonSignup, R.color.colorPrimaryClick, R.color.colorPrimary);

        editTextPhone.setTypeface(mGlobals.setFont(mGlobals.mContext, Constants.robotolight));
        editTextAddress.setTypeface(mGlobals.setFont(mGlobals.mContext, Constants.robotolight));
        editTextAddress2.setTypeface(mGlobals.setFont(mGlobals.mContext, Constants.robotolight));
        editTextUsername.setTypeface(mGlobals.setFont(mGlobals.mContext, Constants.robotolight));
        editTextPassword.setTypeface(mGlobals.setFont(mGlobals.mContext, Constants.robotolight));
        textInputLayoutPassword.setTypeface(mGlobals.setFont(mGlobals.mContext, Constants.robotolight));
        textInputLayoutAddress.setTypeface(mGlobals.setFont(mGlobals.mContext, Constants.robotolight));
        textInputLayoutAddress2.setTypeface(mGlobals.setFont(mGlobals.mContext, Constants.robotolight));
        textInputLayoutUsername.setTypeface(mGlobals.setFont(mGlobals.mContext, Constants.robotolight));
        textInputLayoutPhone.setTypeface(mGlobals.setFont(mGlobals.mContext, Constants.robotolight));
        buttonSignup.setTypeface(mGlobals.setFont(mGlobals.mContext, Constants.robotolight));
        textViewCode.setTypeface(mGlobals.setFont(mGlobals.mContext, Constants.robotolight));
        textViewHeading.setTypeface(mGlobals.setFont(mGlobals.mContext, Constants.robotolight));

    }


    private void initActivity() {

        ButterKnife.bind(this);
        mGlobals = Globals.getInstance(Signup.this);

        progressDialog = new ProgressDialog(mGlobals.mContext);
        progressDialog.setMessage("Registering ...");
        progressDialog.setCancelable(false);

    }

    @OnClick(R.id.btn_signup)
    public void onSignup(View view) {
        MyApplication.getInstance().trackEvent("Button Clicked", "Sign Up ", "New User");
        try {
            if (isValid()) {

                Log.d("sign Up", "user signup");
                progressDialog.show();
                HashMap<String, String> params = new HashMap<>();
                params.put("name", name);
                params.put("email", "");
                params.put("password", password);
                params.put("phone_number", phone);
                params.put("address", addressFromLocation);
                params.put("address_line_2", address);
                if (mGlobals.appSession.hasVendor()) {
                    params.put("location_coordinates", mGlobals.appSession.getCoordinates());
                    // Log.d("llooooooo_if",mGlobals.appSession.getCoordinates());

                } else {
                    params.put("location_coordinates", lati + "," + longi);
                    //Log.d("llooooooo_else", lati + "," + longi);
                }

                params.put("push_token", mGlobals.gcmToken);
                Log.d("findingissue", params.toString());
                Constants.apiPostRequest(mGlobals.mContext, Request.Method.POST, Constants.URL_REGISTER, params, 0, new ResponseListenerForApiCall() {
                    @Override
                    public void onError(VolleyError error, int fromWhere) {
                        Log.d("findingissue", error.toString());
                        progressDialog.dismiss();
                        try {
                            if (error instanceof AuthFailureError
                                    || error instanceof ParseError) {
                                AlertDialogs.getInstance().showDialogOK(mGlobals.mContext, Constants.SERVER_HEADING, Constants.SERVER_DESC, null, false);
                                //TODO
                            } else if (error instanceof NetworkError
                                    || error instanceof TimeoutError
                                    || error instanceof NoConnectionError) {
                                AlertDialogs.getInstance().showDialogOK(mGlobals.mContext, Constants.INTERNET_HEADING, Constants.INTERNET_DESC, null, false);
                            } else
                                AlertDialogs.getInstance().showDialogOK(mGlobals.mContext, Constants.SERVER_HEADING, Constants.SERVER_DESC, null, false);
                        } catch (WindowManager.BadTokenException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onSuccess(JSONObject jsonObject, int fromWhere) {
                        progressDialog.dismiss();
                        Log.d("findingissue", jsonObject.toString());
                        if (jsonObject != null) {
                            try {
                                if (jsonObject.has("error")) {
                                    MyApplication.getInstance().trackScreenView("User Sign Up ");
                                    Iterator<?> keys = jsonObject.getJSONObject("error").keys();
                                    String errors = "";
                                    while (keys.hasNext()) {
                                        String key = (String) keys.next();
                                        for (int i = 0; i < jsonObject.getJSONObject("error").getJSONArray(key).length(); i++)
                                            errors = errors + "&#8226; " + jsonObject.getJSONObject("error").getJSONArray(key).getString(i) + "<br/>";
                                    }
                                    AlertDialogs.getInstance().showDialogOK(mGlobals.mContext, "Error(s)", Html.fromHtml(errors).toString(), null, false);
                                } else {
                                    Customer customer = new Gson().fromJson(jsonObject.getJSONObject("customer").toString(),
                                            Customer.class);
                                    customer.setUserToken(jsonObject.has("token") ? jsonObject.getString("token") : "");
                                    customer.setPassword(password);
                                    mGlobals.customer = new Customer(customer);
                                    MyApplication.getInstance().facebookEventTracking("SignUp Successfully");
                                    mGlobals.appSession.createLoginSession(jsonObject, password);
                                    startActivity(new Intent(mGlobals.mContext, CategoriesList.class));
                                    if (SelectActivity.selectActivityInstance != null)
                                        SelectActivity.selectActivityInstance.finish();
                                    finish();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }

                    @Override
                    public void onStatusCodeError(int statusCode) {
                        progressDialog.dismiss();
                    }
                });
            }
        } catch (WindowManager.BadTokenException e) {
            e.printStackTrace();
        }

    }

    private String name = "", phone = "", address = "", password = "";

    private boolean isValid() {

        name = editTextUsername.getText().toString().trim();
        phone = editTextPhone.getText().toString().trim();
        address = editTextAddress2.getText().toString().trim();
        password = editTextPassword.getText().toString().trim();

        if (editTextUsername.getText().toString().trim().isEmpty()) {
            editTextUsername.setError("Enter name to proceed");
            return false;
        } else if (editTextPhone.getText().toString().trim().isEmpty()) {
            editTextPhone.setError("Enter phone number to proceed");
            return false;
        } else if (editTextPhone.getText().toString().trim().length() != 10) {
            editTextPhone.setError("Phone number must be 10 characters");
            return false;
        } else if (editTextPhone.getText().toString().trim().charAt(0) == '0') {
            editTextPhone.setError("First digit cannot be 0");
            return false;
        } else if (addressFromLocation == null || addressFromLocation.equals("")) {
            editTextAddress.setError("Enter address to proceed");
            return false;
        } else if (editTextAddress2.getText().toString().trim().isEmpty()) {
            editTextAddress2.setError("Enter address to proceed");
            return false;
        } else if (editTextPassword.getText().toString().trim().isEmpty()) {
            editTextPassword.setError("Enter password to proceed");
            return false;
        } else if (editTextPassword.getText().toString().length() < 6) {
            editTextPassword.setError("Password must be min 6 characters");
            return false;
        }

        return true;
    }

}
