package com.push.sampleproject.interfaces;

import com.barfee.mart.models.Product;

/**
 * Created by Haider on 1/22/2016.
 */
public interface FragProdObserver {
    void update(Product product);
    void reset();
}