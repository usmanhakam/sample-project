package com.push.sampleproject.interfaces;

/**
 * Created by Haider on 1/22/2016.
 */
public interface CartObserver {
    public void update(boolean isCheckout);
}