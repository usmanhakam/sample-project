package com.push.sampleproject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.barfee.mart.fragments.FragmentProducts;
import com.barfee.mart.interfaces.FragProdObserver;
import com.barfee.mart.models.Category;
import com.barfee.mart.models.Product;
import com.barfee.mart.utils.AlertDialogs;
import com.barfee.mart.utils.Constants;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.ogaclejapan.smarttablayout.SmartTabLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.barfee.mart.utils.Constants.FREE_DELIVERY_PRICE;

public class ProductMenu extends BaseActivityHavingCart {

    private static final int BACK_FROM_SEARCH = 101;

    @Bind(R.id.materialTabHost)
    SmartTabLayout tabHost;
    @Bind(R.id.viewpager)
    ViewPager pager;
    @Bind(R.id.app_bar)
    Toolbar toolbar;
    @Bind(R.id.tv_title)
    TextView titleToolbar;

    @Bind(R.id.iv_vendor_image)
    ImageView imageViewVendor;
    @Bind(R.id.tv_vendor_name)
    TextView textViewVendorName;
    @Bind(R.id.tv_delivery_label)
    TextView textViewDeliveryLabel;
    @Bind(R.id.tv_delivery)
    TextView textViewDelivery;
    @Bind(R.id.tv_free_delivery_label)
    TextView textViewFreeDeliveryLabel;
    @Bind(R.id.tv_free_delivery)
    TextView textViewFreeDelivery;

//    @Bind(R.id.backdrop)
//    ImageView imageViewBackDrop;
//    @Bind(R.id.collapsing_toolbar)
//    CollapsingToolbarLayout collapsingToolbarLayout;
//    @Bind(R.id.appbar)
//    AppBarLayout appBarLayout;

    ViewPagerAdapter pagerAdapter;
    private Category mainCategory;
    public ArrayList<String> title;
    private boolean isPopupOpen = false;
    private PopupWindow pwindow;
    private ProgressDialog progressDialog;
    private DisplayImageOptions options;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_product_menu);
        MyApplication.getInstance().facebookEventTracking("On Product Menu");
        ButterKnife.bind(this);
        super.onCreate(savedInstanceState);

        try {
            initToolbar();
            initActivity();
            initPager();
            initViews();
            setFooterListener(true);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

    private void initToolbar() {

        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_chevron_left_white_36dp);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mainCategory = (Category) getIntent().getSerializableExtra("category");
        titleToolbar.setText(mainCategory.label);
        MyApplication.getInstance().trackEvent("Category ",mainCategory.label,"");
        setTitle("");


//        setTitle(mainCategory.label);

//        String url = (mainCategory.image);

//        ImageLoader.getInstance().displayImage(url, imageViewBackDrop, options, null);

//        Glide.with(this)
//                .load(url)
//                .centerCrop()
//                .crossFade()
//                .into(imageViewBackDrop);


//        Typeface tf = mGlobals.setFont(mGlobals.mContext, Constants.robotobold);
//        collapsingToolbarLayout.setCollapsedTitleTypeface(tf);
//        collapsingToolbarLayout.setExpandedTitleColor(ContextCompat.getColor(mGlobals.mContext, R.color.white));
//        collapsingToolbarLayout.setCollapsedTitleTextColor(ContextCompat.getColor(mGlobals.mContext, R.color.white));
//        collapsingToolbarLayout.setExpandedTitleTypeface(tf);

    }

    private void initActivity() {

        progressDialog = new ProgressDialog(mGlobals.mContext);
        progressDialog.setMessage("Loading cart for checkout...");

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.defaultimage)
                .showImageForEmptyUri(R.drawable.defaultimage)
                .showImageOnFail(R.drawable.defaultimage)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

        textViewDeliveryLabel.setTypeface(mGlobals.setFont(this, Constants.robotolight));
        textViewDelivery.setTypeface(mGlobals.setFont(this, Constants.robotolight));
        textViewFreeDelivery.setTypeface(mGlobals.setFont(this, Constants.robotolight));
        textViewFreeDeliveryLabel.setTypeface(mGlobals.setFont(this, Constants.robotolight));
        textViewVendorName.setTypeface(mGlobals.setFont(this, Constants.robotobold));

        textViewVendorName.setText(mGlobals.appSession.getVendorName());// + " - " + mGlobals.appSession.getVendorAddress());
        textViewDelivery.setText(mGlobals.appSession.getVendorMinutes() + " minutes");
        textViewFreeDelivery.setText(Constants.PRICE_UNIT + FREE_DELIVERY_PRICE + Constants.PRICE_UNIT_END);
        ImageLoader.getInstance().displayImage(mGlobals.appSession.getVendorImage(), imageViewVendor, options, null);

    }

    @Override
    protected void initViews() {
        super.initViews();
//        CoordinatorLayout.LayoutParams layoutParams = new CoordinatorLayout.LayoutParams(CoordinatorLayout.LayoutParams.MATCH_PARENT, (int) (mGlobals.mScreenHeight * 0.32));
//        appBarLayout.setLayoutParams(layoutParams);
    }

    private void initPager() {

        title = new ArrayList<>();
        if (mainCategory.subCategories.size() == 0) {
            tabHost.setVisibility(View.GONE);
            title.add(mainCategory.label);
        } else {
            tabHost.setVisibility(View.VISIBLE);
            for (int i = 0; i < mainCategory.subCategories.size(); i++)
                title.add(mainCategory.subCategories.get(i).label);
        }

        // init view pager
        pagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(pagerAdapter);
//        pager.getParent().requestDisallowInterceptTouchEvent(true);
        tabHost.setViewPager(pager);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_category, menu);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case BACK_FROM_SEARCH:
                    HashMap<Integer, Product> map = (HashMap<Integer, Product>) data.getSerializableExtra("array");
                    if (map != null)
                        for (Map.Entry<Integer, Product> entry : map.entrySet()) {
                            pagerAdapter.notifyAllObservers(entry.getValue());
                        }
                    break;
            }
        }
    }

    @OnClick(R.id.ll_footer_data)
    public void checkOut(View v) {
        try {
            if (mGlobals.appSession.isLoggedIn()) {
                startActivityForResult(new Intent(this, CartDetail.class), BACK_FROM_SEARCH);
            } else
                AlertDialogs.getInstance().showDialogOK(mGlobals.mContext, "Not registered", "You are not a member", null, false);
//            Log.v("quantity : price", CartHelper.getCart().totalQuantity() + ":" + CartHelper.getCart().totalPrice());
        } catch (WindowManager.BadTokenException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.menu_search:
                startActivityForResult(new Intent(this, SearchActivity.class), BACK_FROM_SEARCH);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void update(boolean isCheckout) {
        super.update(isCheckout);
        if (isCheckout)
            pagerAdapter.notifyAllObserversCheckout();
    }

    public class ViewPagerAdapter extends FragmentPagerAdapter {

        private List<FragProdObserver> observers = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        public Fragment getItem(int num) {
            if (mainCategory.subCategories.size() == 0) {
                FragmentProducts fragmentProducts = FragmentProducts.init(mainCategory.catId, mainCategory.label);
                observers.add(fragmentProducts);
                return fragmentProducts;
            } else {
                FragmentProducts fragmentProducts = FragmentProducts.init(mainCategory.subCategories.get(num).catId, mainCategory.label);
                observers.add(fragmentProducts);
                return fragmentProducts;
            }

        }

        public void notifyAllObservers(Product product) {
            for (FragProdObserver observer : observers) {
                observer.update(product);
            }
        }

        @Override
        public int getCount() {
            return title.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return title.get(position);
        }


        public void notifyAllObserversCheckout() {
            for (FragProdObserver observer : observers) {
                observer.reset();
            }
        }
    }


}
