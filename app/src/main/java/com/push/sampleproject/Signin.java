package com.push.sampleproject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.barfee.mart.interfaces.ResponseListenerForApiCall;
import com.barfee.mart.models.Customer;
import com.barfee.mart.utils.AlertDialogs;
import com.barfee.mart.utils.Constants;
import com.barfee.mart.utils.Globals;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Signin extends AppCompatActivity {

    @Bind(R.id.et_phone)
    EditText editTextPhone;
    @Bind(R.id.et_password)
    EditText editTextPassword;
    @Bind(R.id.til_phone)
    TextInputLayout textInputLayoutPhone;
    @Bind(R.id.til_password)
    TextInputLayout textInputLayoutPassword;
    @Bind(R.id.btn_signin)
    Button buttonSignin;
    @Bind(R.id.btn_forgot)
    TextView buttonForgot;
    @Bind(R.id.tv_code)
    TextView textViewCode;
    @Bind(R.id.tv_signin_heading)
    TextView textViewHeading;

    private Globals mGlobals;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        MyApplication.getInstance().facebookEventTracking("On SignIn");

        initActivity();
        initViews();
    }

    @OnClick(R.id.tv_signin_heading)
    public void onBack(View v) {
        finish();
    }

    private void initViews() {

        editTextPhone.setTypeface(mGlobals.setFont(mGlobals.mContext, Constants.robotolight));
        editTextPassword.setTypeface(mGlobals.setFont(mGlobals.mContext, Constants.robotolight));
        textInputLayoutPassword.setTypeface(mGlobals.setFont(mGlobals.mContext, Constants.robotolight));
        buttonForgot.setTypeface(mGlobals.setFont(mGlobals.mContext, Constants.robotolight));
        textViewCode.setTypeface(mGlobals.setFont(mGlobals.mContext, Constants.robotolight));
        textInputLayoutPhone.setTypeface(mGlobals.setFont(mGlobals.mContext, Constants.robotolight));
        textViewHeading.setTypeface(mGlobals.setFont(mGlobals.mContext, Constants.robotolight));
        buttonSignin.setTypeface(mGlobals.setFont(mGlobals.mContext, Constants.robotolight));

        mGlobals.setSelector(mGlobals.mContext, buttonSignin, R.color.colorPrimaryClick, R.color.colorPrimary);
    }


    private void initActivity() {

        ButterKnife.bind(this);

        mGlobals = Globals.getInstance(this);



        progressDialog = new ProgressDialog(mGlobals.mContext);
        progressDialog.setMessage("Logging in ...");
        progressDialog.setCancelable(false);

        if (Constants.B_HARCODE) {
            editTextPassword.setText("qwerty");
            editTextPhone.setText("3364245979");
        }

    }

    @OnClick(R.id.btn_signin)
    public void signIn(View v) {
        MyApplication.getInstance().trackEvent("Button Clicked","Sign In ","User Login");

        if (editTextPhone.getText().toString().trim().isEmpty()) {
            editTextPhone.setError("Enter your phone");
        } else if (editTextPassword.getText().toString().trim().isEmpty()) {
            editTextPassword.setError("Enter your password");
        } else if (editTextPhone.getText().toString().trim().length() != 10) {
            editTextPhone.setError("Phone number must be 10 characters");
        } else {
            progressDialog.show();
            String url = Constants.URL_SIGNIN;
            HashMap<String, String> params = new HashMap<>();
            params.put("phone_number", editTextPhone.getText().toString().trim());
            params.put("password", editTextPassword.getText().toString().trim());
            params.put("push_token", mGlobals.gcmToken);
            Constants.apiPostRequest(mGlobals.mContext, Request.Method.POST, url, params, 0, new ResponseListenerForApiCall() {
                @Override
                public void onSuccess(JSONObject jsonObject, int fromWhere) {
                    Log.d("rrrrrrr",jsonObject.toString());
                    progressDialog.dismiss();

                    try {
                        if (jsonObject != null) {
                            if (jsonObject.has("error")) {
                                String message = jsonObject.getString("error");
                                MyApplication.getInstance().trackScreenView("User Sign In ");
                                AlertDialogs.getInstance().showDialogOK(mGlobals.mContext, "Error", message, null, false);
                            } else {
                                if (jsonObject.has("customer")) {
                                    Customer customer = new Gson().fromJson(jsonObject.getJSONObject("customer").toString(),
                                            Customer.class);
                                    customer.setUserToken(jsonObject.has("token") ? jsonObject.getString("token") : "");
                                    customer.setPassword(editTextPassword.getText().toString().trim());
                                    if (customer.getLocationCoordinates() != null
                                            && !customer.getLocationCoordinates().equals("")) {
                                        mGlobals.appSession.createLoginSession(jsonObject, customer.getPassword());
                                        MyApplication.getInstance().facebookEventTracking("SignIn Successfully");
                                        Intent intent = new Intent(mGlobals.mContext, CategoriesList.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        finish();

                                        if (SelectActivity.selectActivityInstance != null)
                                            SelectActivity.selectActivityInstance.finish();
                                        finish();
                                    } else {

                                        mGlobals.customer = new Customer(customer);
                                        Toast.makeText(mGlobals.mContext, "Your location is not set", Toast.LENGTH_SHORT).show();
                                        mGlobals.fromWhere = Constants.SIGN_IN;
                                        startActivity(new Intent(mGlobals.mContext, DeliveryLocation.class));
                                        finish();
                                    }
                                } else {
                                    AlertDialogs.getInstance().showDialogOK(mGlobals.mContext, Constants.SERVER_HEADING,
                                            Constants.SERVER_DESC, null, false);
                                }
                            }
                        }
                    } catch (WindowManager.BadTokenException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        AlertDialogs.getInstance().showDialogOK(mGlobals.mContext, Constants.SERVER_HEADING,
                                Constants.SERVER_DESC, null, false);
                    }
                }

                @Override
                public void onError(VolleyError error, int fromWhere) {
                    try {
                        Log.d("rrrrrrr",error.toString());
                        progressDialog.dismiss();
                        if (error instanceof AuthFailureError
                                || error instanceof ParseError) {
                            AlertDialogs.getInstance().showDialogOK(mGlobals.mContext, Constants.SERVER_HEADING,
                                    Constants.SERVER_DESC, null, false);
                            //TODO
                        } else if (error instanceof NetworkError
                                || error instanceof TimeoutError
                                || error instanceof NoConnectionError) {
                            AlertDialogs.getInstance().showDialogOK(mGlobals.mContext, Constants.INTERNET_HEADING,
                                    Constants.INTERNET_DESC, null, false);
                        } else

                            AlertDialogs.getInstance().showDialogOK(mGlobals.mContext, Constants.SERVER_HEADING, Constants.SERVER_DESC, null, false);
                    } catch (WindowManager.BadTokenException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onStatusCodeError(int statusCode) {
                    progressDialog.dismiss();
                }
            });
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.btn_forgot)
    public void forgot(View v) {

        MyApplication.getInstance().trackEvent("Button Clicked","UserForgotPassword","");

        startActivity(new Intent(mGlobals.mContext, ForgotPassword.class));
    }
}
