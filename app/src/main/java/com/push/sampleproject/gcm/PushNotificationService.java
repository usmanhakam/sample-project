package com.push.sampleproject.gcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;

import com.barfee.mart.CategoriesList;
import com.barfee.mart.OrderHistory;
import com.barfee.mart.R;
import com.barfee.mart.utils.AppSession;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import io.intercom.android.sdk.push.IntercomPushClient;

/**
 * Created by Haider on 2/1/2016.
 */
public class PushNotificationService extends FirebaseMessagingService {

    private static final String TAG = "FirebaseNoti";
    private NotificationManager notificationManager;
    private NotificationCompat.BigPictureStyle notiStyle;
    private final IntercomPushClient intercomPushClient = new IntercomPushClient();

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
        try {
            // Check if message contains a data payload.
            if (remoteMessage.getData().size() > 0)
                Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            AppSession session = new AppSession(this, AppSession.PREF_APP);
            if (session.isLoggedIn()) {
                if (remoteMessage.getData() != null) {
                    if (intercomPushClient.isIntercomPush(remoteMessage.getData())) {
                        intercomPushClient.handlePush(getApplication(), remoteMessage.getData());
                    } else {
                        String title = remoteMessage.getData().get("title");
                        String description = remoteMessage.getData().get("body");
                        String image = remoteMessage.getData().get("img");
                        String action = remoteMessage.getData().get("action");
                        Log.v("my notification", title + "\n" + description + "\n" + image + "\n" + action);
                        if (image != null && !image.equals("")) {
                            issueNotification(title, description, image, action);
                        } else {
                            issueNotification(title, description, action);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void issueNotification(final String title, final String message, String image, final String action) {

        notificationManager = (NotificationManager)
                getSystemService(NOTIFICATION_SERVICE);
        notiStyle = new NotificationCompat.BigPictureStyle();

        ImageLoader.getInstance().loadImage(image, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                // Do whatever you want with Bitmap
                if (loadedImage != null)
                    notiStyle.bigPicture(loadedImage);
                // Constructs the Builder object.
                NotificationCompat.Builder builder = new NotificationCompat.Builder(
                        PushNotificationService.this);
    /*
     * Clicking the notification itself displays ResultActivity, which provides
     * UI for snoozing or dismissing the notification.
     * This is available through either the normal view or big view.
     */
                Class<?> className;
                if (action.equals("home"))
                    className = CategoriesList.class;
                else
                    className = OrderHistory.class;
                Intent resultIntent = new Intent(PushNotificationService.this, className);
                resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                // Because clicking the notification opens a new ("special") activity, there's
                // no need to create an artificial back stack.
                PendingIntent resultPendingIntent =
                        PendingIntent.getActivity(
                                PushNotificationService.this,
                                0,
                                resultIntent,
                                PendingIntent.FLAG_UPDATE_CURRENT
                        );

                notiStyle.setSummaryText(message);
                builder.setContentIntent(resultPendingIntent);
                Notification notification = builder.setSmallIcon(R.drawable.intercom_push_icon)
                        .setContentTitle(title)
                        .setStyle(notiStyle)
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setLargeIcon(BitmapFactory.decodeResource(PushNotificationService.this.getResources(), R.mipmap.ic_launcher))
                        .setContentText(message).build();
                notification.flags |= Notification.FLAG_AUTO_CANCEL;
                notificationManager.notify((int) System.currentTimeMillis(), notification);
            }
        });

    }


    private void issueNotification(String title, String message, String action) {

        NotificationManager mNotificationManager = (NotificationManager)
                getSystemService(NOTIFICATION_SERVICE);
        // Constructs the Builder object.
        NotificationCompat.Builder builder = new NotificationCompat.Builder(
                this);
    /*
     * Clicking the notification itself displays ResultActivity, which provides
     * UI for snoozing or dismissing the notification.
     * This is available through either the normal view or big view.
     */
        Class<?> className;
        if (action.equals("home"))
            className = CategoriesList.class;
        else
            className = OrderHistory.class;
        Intent resultIntent = new Intent(PushNotificationService.this, className);
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        // Because clicking the notification opens a new ("special") activity, there's
        // no need to create an artificial back stack.
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        this,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );


        builder.setContentIntent(resultPendingIntent);
        Notification notification = builder.setSmallIcon(R.drawable.intercom_push_icon)
                .setContentTitle(title)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setLargeIcon(BitmapFactory.decodeResource(this.getResources(), R.mipmap.ic_launcher))
                .setContentText(message).build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        mNotificationManager.notify((int) System.currentTimeMillis(), notification);
    }

}