package com.push.sampleproject;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.barfee.mart.interfaces.ResponseListenerForApiCall;
import com.barfee.mart.models.Customer;
import com.barfee.mart.server.DeleteTokenService;
import com.barfee.mart.server.GCMTokenService;
import com.barfee.mart.utils.AlertDialogs;
import com.barfee.mart.utils.Constants;
import com.barfee.mart.utils.Globals;
import com.google.gson.Gson;

import net.hockeyapp.android.CrashManager;
import net.hockeyapp.android.UpdateManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import io.intercom.android.sdk.Intercom;
import io.intercom.android.sdk.identity.Registration;

public class Splash extends AppCompatActivity {

    private Globals mGlobals;
    public JSONObject withoutLoginJson;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        MyApplication.getInstance().facebookEventTracking("On Splash");
        mGlobals = Globals.getInstance(Splash.this);

        checkForUpdates();

      //  AppEventsLogger logger = AppEventsLogger.newLogger(this);
       // logger.logEvent("I am at splash");
        //MyApplication.getInstance().facebookEventTracking("Testing "+"Splash");

//        if (!mGlobals.appSession.isLoggedIn())
        startService(new Intent(mGlobals.mContext, GCMTokenService.class));
        startService(new Intent(mGlobals.mContext, DeleteTokenService.class));

        checkVersion();
//        VideoView view = (VideoView) findViewById(R.id.video_view);
//        String path = "android.resource://" + getPackageName() + "/" + R.raw.splash;
//        view.setVideoURI(Uri.parse(path));
//        view.start();
//        view.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//            @Override
//            public void onCompletion(MediaPlayer mp) {
//                mp.stop();
//                checkVersion();
//            }
//        });

        //Add code to print out the key hash
//        try {
//            PackageInfo info = getPackageManager().getPackageInfo(
//                    "com.barfee.mart",
//                    PackageManager.GET_SIGNATURES);
//            for (Signature signature : info.signatures) {
//                MessageDigest md = MessageDigest.getInstance("SHA");
//                md.update(signature.toByteArray());
//                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
//            }
//        } catch (PackageManager.NameNotFoundException e) {
//
//        } catch (NoSuchAlgorithmException e) {
//
//        }

    }

    private void checkVersion() {


        Constants.apiGetRequest(mGlobals.mContext, Request.Method.GET, Constants.URL_CHECK_VERISON, null, 0, new ResponseListenerForApiCall() {
            @Override
            public void onSuccess(JSONObject jsonObject, int fromWhere) {
                if (jsonObject != null) {
                    try {
                        int version = jsonObject.has("version") ? jsonObject.getInt("version") : -1;
                        if (version != -1) {
                            int versionCode = BuildConfig.VERSION_CODE;
                            if (version > versionCode) {
                                showDialogForUpdate();
                            } else continueToApp();
                        } else continueToApp();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        continueToApp();
                    }
                }
            }

            @Override
            public void onError(VolleyError volleyError, int fromWhere) {
                continueToApp();
            }

            @Override
            public void onStatusCodeError(int statusCode) {
                continueToApp();
            }
        });

    }

    private void continueToApp() {



        if (mGlobals.appSession.isLoggedIn()) {
            getCustomerObject();
//            //version >29 & flag
//            if (BuildConfig.VERSION_CODE > 29 && !mGlobals.allLogoutPref.getAllLogout()) {
//                mGlobals.allLogoutPref.setAllLogout(true);
//                mGlobals.appSession.logoutUser();
//                CartHelper.getCart().clear();
//                startActivity(new Intent(mGlobals.mContext, Signin.class));
//            } else
        }else if (mGlobals.appSession.hasVendor()){
            updateVendor();
            //startActivity(new Intent(mGlobals.mContext, CategoriesList.class));
            //finish();
        } else {
            startActivity(new Intent(mGlobals.mContext, DeliveryLocation.class));
            finish();
//            CartHelper.getCart().clear();
//            mGlobals.appSession.logoutUser();
//            mGlobals.allLogoutPref.setAllLogout(true);
        }
    }

    private void getCustomerObject() {
        String url = String.format(Constants.URL_CUSTOMER_INFO, mGlobals.appSession.getToken());
        Constants.apiGetRequest(mGlobals.mContext, Request.Method.GET, url, null, 0, new ResponseListenerForApiCall() {
            @Override
            public void onSuccess(JSONObject jsonObject, int fromWhere) {
                try {
                    if (jsonObject != null) {
                        Log.d("ssssssssssssssssss", "SPLASH_gET>>>>>>" + jsonObject.toString());
                        Customer customer = new Gson().fromJson(jsonObject.getJSONObject("customer").toString(),
                                Customer.class);
                        customer.setUserToken(jsonObject.has("token") ? jsonObject.getString("token") : "");
                        mGlobals.customer = new Customer(customer);
                        mGlobals.appSession.createLoginSession(jsonObject, "");

                        Intercom.client().registerIdentifiedUser(Registration.create().withUserId(mGlobals.appSession.getPhone()));
                        if (mGlobals.appSession.getCoordinates() == null) {
                            mGlobals.fromWhere = Constants.ALREADY_LOGGED_IN;
                            startActivity(new Intent(mGlobals.mContext, DeliveryLocation.class));
                        } else {
                            startActivity(new Intent(mGlobals.mContext, CategoriesList.class));
                        }
                    } else {
                        startActivity(new Intent(mGlobals.mContext, SelectActivity.class));
                    }
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                    startActivity(new Intent(mGlobals.mContext, SelectActivity.class));
                    finish();
                }
            }

            @Override
            public void onError(VolleyError volleyError, int fromWhere) {
                Intercom.client().registerIdentifiedUser(Registration.create().withUserId(mGlobals.appSession.getPhone()));
                if (mGlobals.appSession.getCoordinates() == null) {
                    mGlobals.fromWhere = Constants.ALREADY_LOGGED_IN;
                    startActivity(new Intent(mGlobals.mContext, DeliveryLocation.class));
                } else {
                    startActivity(new Intent(mGlobals.mContext, CategoriesList.class));
                }
                finish();
            }

            @Override
            public void onStatusCodeError(int statusCode) {
                Intercom.client().registerIdentifiedUser(Registration.create().withUserId(mGlobals.appSession.getPhone()));
                if (mGlobals.appSession.getCoordinates() == null) {
                    mGlobals.fromWhere = Constants.ALREADY_LOGGED_IN;
                    startActivity(new Intent(mGlobals.mContext, DeliveryLocation.class));
                } else {
                    startActivity(new Intent(mGlobals.mContext, CategoriesList.class));
                }
                finish();
            }
        });
    }

    private void showDialogForUpdate() {
        AlertDialogs.getInstance().showDialogYes(mGlobals.mContext, "New Version", "A new app version is available on play store. Do you want to update it now?", new AlertDialogs.OnDialogClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.playStoreUrl)));
                finish();
            }
        }, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        // ... your own onResume implementation
        checkForCrashes();

    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterManagers();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterManagers();
    }

    private void checkForCrashes() {
        CrashManager.register(this);
    }

    private void checkForUpdates() {
        // Remove this for store builds!
        UpdateManager.register(this);
    }

    private void unregisterManagers() {
        UpdateManager.unregister();
    }




   public void updateVendor(){

       if (mGlobals.appSession.getCoordinates() != null) {
           mGlobals.fromWhere =Constants.WITHOUT_LOGIN;
           apiCallForLocation();
       }
   }

    public void apiCallForLocation() {


        HashMap<String, String> params = new HashMap<>();

            params.put("latitude",mGlobals.appSession.getCoordinates().split(",")[0]);
            params.put("longitude",mGlobals.appSession.getCoordinates().split(",")[1]);

        Constants.apiPostRequest(this, Request.Method.POST, Constants.URL_MAP_CHECK_FOR_COORDINATES, params, 0, new ResponseListenerForApiCall() {
            @Override
            public void onSuccess(JSONObject jsonObject, int fromWhere) {
                try {

                    Log.d("Api Call for Laocation", jsonObject.toString());
                    if (jsonObject != null) {
                        if (jsonObject.has("message")) {

                            withoutLoginJson = jsonObject;
                            if (mGlobals.appSession.createVendorWithOutLogin(withoutLoginJson)) {
                                Log.d("ssssssssssssssssss", "SPLASH>>>>>>" + withoutLoginJson.toString());
                                // progressDialog.dismiss();
                                Intent i = new Intent(mGlobals.mContext, CategoriesList.class);
                                startActivity(i);
                                finish();
                            }
                        }

                            } else {
                                String error = jsonObject.has("error") ? jsonObject.getString("error") : "No location";
                                Toast.makeText(mGlobals.mContext, error, Toast.LENGTH_SHORT).show();
                                showChangeLocalityScreen(mGlobals.mContext);
                            }


                    } catch(Exception e){
                        e.printStackTrace();
                    }

                }


            @Override
            public void onError(VolleyError error, int fromWhere) {
                try {
                   // progressDialog.dismiss();
                    if (error instanceof AuthFailureError
                            || error instanceof ParseError) {
                        AlertDialogs.getInstance().showDialogOK(mGlobals.mContext, Constants.SERVER_HEADING, Constants.SERVER_DESC, null, false);
                        //TODO
                    } else if (error instanceof NetworkError
                            || error instanceof TimeoutError
                            || error instanceof NoConnectionError) {
                        AlertDialogs.getInstance().showDialogOK(mGlobals.mContext, Constants.INTERNET_HEADING, Constants.INTERNET_DESC, null, false);
                    } else
                        AlertDialogs.getInstance().showDialogOK(mGlobals.mContext, Constants.SERVER_HEADING, Constants.SERVER_DESC, null, false);
                } catch (WindowManager.BadTokenException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onStatusCodeError(int statusCode) {
               // progressDialog.dismiss();
                AlertDialogs.getInstance().showDialogOK(mGlobals.mContext, Constants.SERVER_HEADING, Constants.SERVER_DESC, null, false);
            }
        });
    }
    public void showChangeLocalityScreen(Context context) {
        Intent intent = new Intent(context, ChangeLocality.class);
        context.startActivity(intent);
        ((Activity) context).finish();
    }
}
