package com.push.sampleproject.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.barfee.mart.Activity3rdTile;
import com.barfee.mart.ProductMenu;
import com.barfee.mart.R;
import com.barfee.mart.models.Category;
import com.barfee.mart.utils.Constants;
import com.barfee.mart.utils.Globals;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Haider on 1/22/2016.
 */
public class RecyclerViewAdapterGridView extends RecyclerView.Adapter<RecyclerViewAdapterGridView.CategoryViewHolder> {
    private DisplayImageOptions options;
    private ArrayList<Category> categoryArrayList;
    private Context mContext;
    private Globals mGlobals;


    public RecyclerViewAdapterGridView(Context context, ArrayList<Category> categoryArrayList) {
        this.categoryArrayList = categoryArrayList;
        this.mContext = context;

        mGlobals = Globals.getInstance((Activity) context);

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.defaultimagelarge)
                .showImageForEmptyUri(R.drawable.defaultimagelarge)
                .showImageOnFail(R.drawable.defaultimagelarge)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

    }

    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.grid_item, viewGroup, false);

        CategoryViewHolder viewHolder = new CategoryViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CategoryViewHolder holder, int i) {
        final Category category = categoryArrayList.get(i);

        CardView.LayoutParams lpImageView = new CardView.LayoutParams((int) (mGlobals.mScreenWidth * 0.5), (int) (mGlobals.mScreenWidth * 0.5));
        holder.imageView.setLayoutParams(lpImageView);
        holder.linearLayoutTexts.setLayoutParams(lpImageView);
        if (category.status == 1) {
            holder.linearLayoutTexts.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.grid_item_gradient));
        } else
            holder.linearLayoutTexts.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.grid_item_gradient_grey_scale));

        String url = (category.image);
//        String url = "http://www.brasil247.com/images/cache/478x326/crop/images%7Ccms-image-000494023.jpg";

        ImageLoader.getInstance().displayImage(url, holder.imageView, options,null);

//        Glide.with(mContext)
//                .load(url)
//                .centerCrop()
//                .placeholder(R.drawable.defaultimagelarge)
//                .crossFade()
//                .into(holder.imageView);
        holder.textViewabel.setText(category.label);
        holder.textViewDesc.setText(category.desc);

    }

    @Override
    public int getItemCount() {
        return (null != categoryArrayList ? categoryArrayList.size() : 0);
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tv_cat_label)
        TextView textViewabel;
        @Bind(R.id.iv_cat_image)
        ImageView imageView;
        @Bind(R.id.tv_cat_desc)
        TextView textViewDesc;
        @Bind(R.id.ll_texts)
        LinearLayout linearLayoutTexts;

        public CategoryViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

            textViewabel.setTypeface(mGlobals.setFont(mContext, Constants.robotobold));
            textViewDesc.setTypeface(mGlobals.setFont(mContext, Constants.robotolight));
        }

        @OnClick(R.id.ll_texts)
        public void onGridItemClick(View view) {
            try {
                Category category = categoryArrayList.get(getAdapterPosition());
                if (category.status == 1) {
                    if (category.subCategories != null && category.subCategories.size() > 0) {  // child
                        if (category.subCategories.get(0).subCategories != null && category.subCategories.get(0).subCategories.size() > 0) {
                            Intent intent = new Intent(mContext, Activity3rdTile.class);
                            intent.putExtra("category", categoryArrayList.get(getAdapterPosition()).subCategories.get(0));
                            ((Activity) mContext).startActivity(intent);
                        } else {
                            Intent intent = new Intent(mContext, ProductMenu.class);
                            intent.putExtra("category", categoryArrayList.get(getAdapterPosition()));
                            ((Activity) mContext).startActivity(intent);
                        }
                    } else {
                        Intent intent = new Intent(mContext, ProductMenu.class);
                        intent.putExtra("category", categoryArrayList.get(getAdapterPosition()));
                        ((Activity) mContext).startActivity(intent);
                    }
                } else
                    Toast.makeText(mContext, "Coming soon!", Toast.LENGTH_SHORT).show();
            } catch (IndexOutOfBoundsException e) {
                e.printStackTrace();
            }
        }
    }
}