package com.push.sampleproject.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.barfee.mart.R;
import com.barfee.mart.SaveOrderListActivity;
import com.barfee.mart.interfaces.OnRecyclerViewRowClicked;
import com.barfee.mart.models.Order;
import com.barfee.mart.utils.Constants;
import com.barfee.mart.utils.Globals;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Haider on 1/22/2016.
 */
public class RecycleViewOrderHistory extends RecyclerView.Adapter<RecycleViewOrderHistory.OrderViewHolder> {
    private ArrayList<Order> OrderArrayList;
    private Context mContext;
    private Globals mGlobals;
    private OnRecyclerViewRowClicked onRecyclerViewRowClicked;

    public RecycleViewOrderHistory(Context context, ArrayList<Order> OrderArrayList, OnRecyclerViewRowClicked onRecyclerViewRowClicked) {
        this.OrderArrayList = OrderArrayList;
        this.mContext = context;
        this.onRecyclerViewRowClicked = onRecyclerViewRowClicked;
        mGlobals = Globals.getInstance((Activity) context);

    }

    @Override
    public OrderViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.order_history_row, viewGroup, false);

        OrderViewHolder viewHolder = new OrderViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(OrderViewHolder holder, int i) {
        final Order order = OrderArrayList.get(i);

        holder.textViewOrderDate.setText(order.date);
        holder.textViewOrderId.setText(order.orderNo);
        holder.textViewOrderAmount.setText(Constants.PRICE_UNIT + order.price + " " + Constants.PRICE_UNIT_END);

        holder.pendingException1.setVisibility(order.pendingExceptionVisibility1);
        holder.pendingException2.setVisibility(order.pendingExceptionVisibility2);
        holder.pendingException3.setVisibility(order.pendingExceptionVisibility3);

        ImageLoader.getInstance().displayImage("drawable://"+order.imageViewPending,holder.imageViewPending);
        ImageLoader.getInstance().displayImage("drawable://"+order.imageViewPendingbar,holder.imageViewPendingbar);

        ImageLoader.getInstance().displayImage("drawable://"+order.imageViewDispatch,holder.imageViewDispatch);
        ImageLoader.getInstance().displayImage("drawable://"+order.imageViewDispatchbar,holder.imageViewDispatchbar);

        ImageLoader.getInstance().displayImage("drawable://"+order.imageViewDelivered,holder.imageViewDelivered);
        ImageLoader.getInstance().displayImage("drawable://"+order.imageViewDeliveredbar,holder.imageViewDeliveredbar);

//        Glide.with(mContext).load(order.imageViewPending).into(holder.imageViewPending);
//        Glide.with(mContext).load(order.imageViewPendingbar).into(holder.imageViewPendingbar);
//
//        Glide.with(mContext).load(order.imageViewDispatch).into(holder.imageViewDispatch);
//        Glide.with(mContext).load(order.imageViewDispatchbar).into(holder.imageViewDispatchbar);
//
//        Glide.with(mContext).load(order.imageViewDelivered).into(holder.imageViewDelivered);
//        Glide.with(mContext).load(order.imageViewDeliveredbar).into(holder.imageViewDeliveredbar);

        holder.textViewPendingText.setTextColor(ContextCompat.getColor(mContext, order.textViewPendingText));
        holder.textViewDeliveredText.setTextColor(ContextCompat.getColor(mContext, order.textViewDeliveredText));
        holder.textViewDispatchText.setTextColor(ContextCompat.getColor(mContext, order.textViewDispatchText));


    }

    @Override
    public int getItemCount() {
        return (null != OrderArrayList ? OrderArrayList.size() : 0);
    }

    public class OrderViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tv_orderid)
        TextView textViewOrderId;
        @Bind(R.id.tv_orderamount)
        TextView textViewOrderAmount;
        @Bind(R.id.tv_orderdate)
        TextView textViewOrderDate;
        @Bind(R.id.tv_orderid_heading)
        TextView textViewOrderIdHeading;
        @Bind(R.id.tv_orderamount_heading)
        TextView textViewOrderAmountHeading;

        @Bind(R.id.tv_pending_text)
        TextView textViewPendingText;
        @Bind(R.id.tv_dispatch_text)
        TextView textViewDispatchText;
        @Bind(R.id.tv_delivered_text)
        TextView textViewDeliveredText;
        @Bind(R.id.iv_pending_image)
        ImageView imageViewPending;
        @Bind(R.id.iv_dispatch_image)
        ImageView imageViewDispatch;
        @Bind(R.id.iv_delivered_image)
        ImageView imageViewDelivered;
        @Bind(R.id.iv_pending_bar)
        ImageView imageViewPendingbar;
        @Bind(R.id.iv_dispatch_bar)
        ImageView imageViewDispatchbar;
        @Bind(R.id.iv_delivered_bar)
        ImageView imageViewDeliveredbar;

        @Bind(R.id.tv_pending_exception1)
        TextView pendingException1;
        @Bind(R.id.tv_pending_exception2)
        TextView pendingException2;
        @Bind(R.id.tv_pending_exception3)
        TextView pendingException3;

        @Bind(R.id.iv_menu_dots)
        ImageView imageViewDots;

        public OrderViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
//            pendingException2.setTypeface(mGlobals.setFont(mContext, Constants.robotolight));
            textViewOrderDate.setTypeface(mGlobals.setFont(mContext, Constants.robotolight));
            textViewOrderId.setTypeface(mGlobals.setFont(mContext, Constants.robotolight));
            textViewDeliveredText.setTypeface(mGlobals.setFont(mContext, Constants.robotolight));
            textViewDispatchText.setTypeface(mGlobals.setFont(mContext, Constants.robotolight));
            textViewPendingText.setTypeface(mGlobals.setFont(mContext, Constants.robotolight));
            textViewOrderAmount.setTypeface(mGlobals.setFont(mContext, Constants.robotolight));
            textViewOrderAmountHeading.setTypeface(mGlobals.setFont(mContext, Constants.robotolight));
            textViewOrderIdHeading.setTypeface(mGlobals.setFont(mContext, Constants.robotolight));

        }

        @OnClick(R.id.ll_history_main)
        public void onRowClick(View view) {
            try {
                onRecyclerViewRowClicked.onRowClicked(getAdapterPosition(), OrderArrayList.get(getAdapterPosition()));
            } catch (IndexOutOfBoundsException e) {
                e.printStackTrace();
            }
        }

        @OnClick(R.id.iv_menu_dots)
        public void onDotClick(View view) {
            PopupMenu popup = new PopupMenu(mGlobals.mContext, view);
            popup.getMenuInflater().inflate(R.menu.menu_dot, popup.getMenu());
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    if (item.getItemId() == R.id.nav_save) {
                        try {
                            Intent intent = new Intent(mGlobals.mContext, SaveOrderListActivity.class);
                            intent.putExtra("order", OrderArrayList.get(getAdapterPosition()));
                            mGlobals.mContext.startActivity(intent);
                        } catch (IndexOutOfBoundsException e) {
                            e.printStackTrace();
                        }
                        return true;
                    }
                    return false;
                }
            });
            popup.show();
        }
    }
}