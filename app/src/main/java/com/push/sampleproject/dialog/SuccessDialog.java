package com.push.sampleproject.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.barfee.mart.R;
import com.barfee.mart.utils.Constants;
import com.barfee.mart.utils.Globals;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Haider on 31/08/2016.
 */
public class SuccessDialog {
    private Dialog d;

    @Bind(R.id.iv_cross)
    ImageView imageViewCross;
    @Bind(R.id.tv_thankyou)
    TextView textViewThank;
    @Bind(R.id.tv_thankyou_detail)
    TextView textViewDetail;
    @Bind(R.id.btn_ok)
    Button buttonOk;

    public SuccessDialog() {
        View v = Globals.getUsage().mActivity.getLayoutInflater().inflate(R.layout.dialog_success, null);
        ButterKnife.bind(this, v);
        d = new AlertDialog.Builder(Globals.getUsage().mContext)
                .setCancelable(true)
                .setView(v)
                .create();

        d.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        d.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(final DialogInterface dialog) {

                buttonOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        d.dismiss();
                    }
                });

                imageViewCross.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        d.dismiss();
                    }
                });


            }
        });

        textViewThank.setTypeface(Globals.getUsage().setFont(Globals.getUsage().mContext, Constants.robotolight));
        textViewDetail.setTypeface(Globals.getUsage().setFont(Globals.getUsage().mContext, Constants.robotobold));
        buttonOk.setTypeface(Globals.getUsage().setFont(Globals.getUsage().mContext, Constants.robotolight));

        Globals.getUsage().setSelector(Globals.getUsage().mContext, buttonOk, R.color.colorPrimaryClick, R.color.colorPrimary);

    }

    public void show() {
        if (d != null)
            d.show();
    }

    public interface OnDialogSelectListener {
        void onSubmit(String... params);
    }

}
