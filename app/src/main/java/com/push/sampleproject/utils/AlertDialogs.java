package com.push.sampleproject.utils;


import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

public class AlertDialogs {

    private static AlertDialogs instance = null;

    private AlertDialogs() {
    }

    public static AlertDialogs getInstance() {
        if (instance == null) {
            instance = new AlertDialogs();
        }
        return instance;
    }

    public void showDialogYesNo(Context context, String title, String message, OnDialogClickListener onClickPositiveButton, OnDialogClickListener onClickNegativeButton, boolean bIsCancelable) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (title != null) {
            builder.setTitle(title);
        }
        builder.setMessage(message);
        builder.setPositiveButton("Yes", onClickPositiveButton);
        builder.setNegativeButton("No", onClickNegativeButton);
        builder.setCancelable(bIsCancelable);
        builder.create().show();
    }

    public void showDialogOK(Context context, String title, String message, OnDialogClickListener onClickPositiveButton, boolean bIsCancelable) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (title != null) {
            builder.setTitle(title);
        }
        builder.setMessage(message);
        builder.setPositiveButton("OK", onClickPositiveButton);
        builder.setCancelable(bIsCancelable);
        builder.create().show();
    }

    public void showDialogYes(Context context, String title, String message, OnDialogClickListener onClickPositiveButton, boolean bIsCancelable) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (title != null) {
            builder.setTitle(title);
        }
        builder.setMessage(message);
        builder.setPositiveButton("Yes", onClickPositiveButton);
        builder.setCancelable(bIsCancelable);
        builder.create().show();
    }


    public void showDialogOKv4(Context context, String title, String message, OnDialogClickListener onClickPositiveButton, boolean bIsCancelable) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        if (title != null) {
            builder.setTitle(title);
        }
        builder.setMessage(message);
        builder.setPositiveButton("OK", onClickPositiveButton);
        builder.setCancelable(bIsCancelable);
        builder.create().show();
    }


    public interface OnDialogClickListener extends DialogInterface.OnClickListener {
    }
}