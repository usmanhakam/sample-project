package com.push.sampleproject.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.Log;
import android.view.WindowManager;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.barfee.mart.interfaces.ResponseListenerForApiCall;
import com.barfee.mart.interfaces.StatusCodeErrorCheck;
import com.barfee.mart.models.Customer;
import com.barfee.mart.server.CustomJSONObjectRequest;
import com.barfee.mart.server.CustomVolleyRequestQueue;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

/**
 * Created by Haider on 1/18/2016.
 */
public class Constants {

    public static final boolean B_HARCODE = false;

    public static final double FREE_DELIVERY_PRICE = 500;
    public static final double DELIVERY_CHARGES = 60;

    public static final int START_TIME = 9;
    public static final int END_TIME = 21;
    public static final int DIFFERENCE_IN_SLOT = 3;


    public static final int DISCOUNT_TYPE_FLAT = 0;
    public static final int DISCOUNT_TYPE_PERCENTAGE = 1;


    public static final int SIGN_UP = 1;
    public static final int SIGN_IN = 2;
    public static final int ALREADY_LOGGED_IN = 3;
    public static final int EDIT_PROFILE = 4;
    public static final int MAIN_ACT = 5;
    public static final int WITHOUT_LOGIN = 6;
    //   public static final String robotolight = "fonts/helveticaregular.ttf";
    // public static final String robotobold = "fonts/helveticabold.ttf";
    public static final String robotolight = "fonts/FF Clan Pro Book.ttf";
    public static final String robotobold = "fonts/FF Clan Pro News.ttf";

 // public static final String BASE_URL = "http://grocer-stage.com/"; //testing
  //  public static final String BASE_URL = "http://api.grocerapps.com/"; //live
    public static final String BASE_URL = "http://dev.grocerapps.com/"; //debug
 //  public static String BASE_URL = "http://34.212.75.232/";
    public static final String URL_REGISTER = BASE_URL + "customer/register";
    public static final String URL_CATEGORIES = BASE_URL + "categories/list?vendor_id=";
    public static final String URL_PRODUCTS = BASE_URL + "products/list?token=";
    public static final String URL_IMAGE = BASE_URL + "uploads/";
    public static final String URL_IMAGE_THUMB = "https://s3-us-west-2.amazonaws.com/grocer-product-images-thumbnails/";
    public static final String URL_CHECKOUT = BASE_URL + "orders/make?token=";
    public static final String URL_ORDER_HISTORY = BASE_URL + "orders/history?token=";
    public static final String URL_SEARCH = BASE_URL + "products/search?token=";
    public static final String URL_SIGNIN = BASE_URL + "customer/signin";
    public static final String URL_ORDER_DETAIL = BASE_URL + "orders/show/%s?token=%s";
    public static final String URL_UPDATE = BASE_URL + "customer/update?token=";
    public static final String URL_CHANGE_PASSWORD = BASE_URL + "customer/changePassword?token=";
    public static final String URL_FORGOT_PASSWORD = BASE_URL + "customer/forgotPassword";
    public static final String URL_MY_ORDER_LIST_NAMES = BASE_URL + "list?token=%s";
    public static final String URL_ORDER_LIST = BASE_URL + "list/%s?token=%s";
    public static final String URL_ADD_ORDER_TO_ORDER_LIST = BASE_URL + "list/%s/order?token=%s";
    public static final String URL_SLIDER = BASE_URL + "slides";
    public static final String URL_PRODUCT_DETAIL_SIMILAR = BASE_URL + "products/similar/";
    public static final String URL_PRODUCT_DETAIL = BASE_URL + "products/show/";
    public static final String URL_HELP = BASE_URL + "help";
    public static final String URL_MAP_CHECK_FOR_COORDINATES = BASE_URL + "map-city";
    public static final String URL_CUSTOMER_INFO = BASE_URL + "customer/info?token=%s";
    public static final String URL_CHECK_VERISON = "http://www.grocerapps.com/release.json";
    public static final String playStoreUrl = "https://play.google.com/store/apps/details?id=com.barfee.mart";
    public static final String URL_APPLY_PROMO_CODE = BASE_URL + "customer/promocode?token=";
    public static final String PRICE_UNIT = "Rs. ";
    public static final String PRICE_UNIT_END = "";

    public static final String SERVER_HEADING = "Server Error";
    public static final String INTERNET_HEADING = "Connection Error";
    public static final String SERVER_DESC = "Invalid response from Server.Please contact admin.";
    public static final String INTERNET_DESC = "We could not establish a connection with our servers. Try again when you are connected to the internet.";
    public static final String TOKEN_EXPIRE = "Please logout and login again, your session has been expired";
    public static final String PROJECT_NUMBER = "1004450030693";


    public static void apiPostRequest(Context context, int method, String url, Map<String, String> params, final int fromWhere, final ResponseListenerForApiCall listenerForApiCall) {

        RequestQueue mQueue = CustomVolleyRequestQueue.getInstance(context).getRequestQueue();
        CustomJSONObjectRequest jsonObjRequest = new CustomJSONObjectRequest(context, method, url, params, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                listenerForApiCall.onSuccess(response, fromWhere);
                Log.d("fffffffff","success>>>>>>>>>"+response.toString());
            }
        }
                , new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("fffffffff",error.toString());
                if (error.networkResponse != null && error.networkResponse.statusCode == 500) {
                    listenerForApiCall.onStatusCodeError(error.networkResponse.statusCode);
                } else
                    listenerForApiCall.onError(error, fromWhere);
            }
        });

        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonObjRequest.setRetryPolicy(policy);
        mQueue.add(jsonObjRequest);
    }

    public static void apiPostRequestContent(Context context, int method, String url, Map<String, String> params, String contentType, final int fromWhere, final ResponseListenerForApiCall listenerForApiCall) {


        RequestQueue mQueue = CustomVolleyRequestQueue.getInstance(context).getRequestQueue();
        CustomJSONObjectRequest jsonObjRequest = new CustomJSONObjectRequest(context, method, url, params, contentType, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                listenerForApiCall.onSuccess(response, fromWhere);
            }
        }
                , new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error.networkResponse != null && error.networkResponse.statusCode == 500) {
                    listenerForApiCall.onStatusCodeError(error.networkResponse.statusCode);
                } else
                    listenerForApiCall.onError(error, fromWhere);
            }
        });
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonObjRequest.setRetryPolicy(policy);
        mQueue.add(jsonObjRequest);
    }

    public static void apiGetRequest(Context context, int method, String url, final Map<String, String> params, final int fromWhere, final ResponseListenerForApiCall listenerForApiCall) {
        RequestQueue mQueue = CustomVolleyRequestQueue.getInstance(context).getRequestQueue();
        CustomJSONObjectRequest jsonRequestPrice = new CustomJSONObjectRequest(context, method, url, params, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                listenerForApiCall.onSuccess(response, fromWhere);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error.networkResponse != null && error.networkResponse.statusCode == 500) {
                    listenerForApiCall.onStatusCodeError(error.networkResponse.statusCode);
                } else
                    listenerForApiCall.onError(error, fromWhere);

            }
        });
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonRequestPrice.setRetryPolicy(policy);
        mQueue.add(jsonRequestPrice);
    }

    public static Date getDateInFormat() {
        String startDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            return df.parse(startDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Date getDateInFormatWithTime(String dateTimeInString) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        try {
            Date date = formatter.parse(dateTimeInString);
            Date newDate = new Date(date.getTime() + 5 * 3600 * 1000);
            return newDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static Date currentTimeOrPromoTime(String dateTime) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            return df.parse(dateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static String formatDateToString(Date date, String format,
                                            String timeZone) {
        // null check
        if (date == null) return null;
        // create SimpleDateFormat object with input format
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        // default system timezone if passed null or empty
        if (timeZone == null || "".equalsIgnoreCase(timeZone.trim())) {
            timeZone = Calendar.getInstance().getTimeZone().getID();
        }
        // set timezone to SimpleDateFormat
        sdf.setTimeZone(TimeZone.getTimeZone(timeZone));
        // return Date in required format with timezone as String
        return sdf.format(date);
    }

    public static void signInAgain(final Globals mGlobals, String phone, final String password, final StatusCodeErrorCheck statusCodeErrorCheck) {
        String url = Constants.URL_SIGNIN;
        HashMap<String, String> params = new HashMap<>();
        params.put("phone_number", phone);
        params.put("password", password);
        Constants.apiPostRequest(mGlobals.mContext, Request.Method.POST, url, params, 0, new ResponseListenerForApiCall() {
            @Override
            public void onSuccess(JSONObject jsonObject, int fromWhere) {
                try {
                    if (jsonObject != null) {
                        if (jsonObject.has("error")) {
                            statusCodeErrorCheck.onstatusCodeCheck(false);
                        } else {
                            Customer customer = new Gson().fromJson(jsonObject.getJSONObject("customer").toString(),
                                    Customer.class);
                            customer.setUserToken(jsonObject.has("token") ? jsonObject.getString("token") : "");
                            customer.setPassword(password);
                            mGlobals.customer = new Customer(customer);
                            if (mGlobals.appSession.createLoginSession(jsonObject, password)) {
                                statusCodeErrorCheck.onstatusCodeCheck(true);
                            } else {
                                statusCodeErrorCheck.onstatusCodeCheck(false);
                            }
                        }
                    } else {
                        statusCodeErrorCheck.onstatusCodeCheck(false);
                    }
                } catch (WindowManager.BadTokenException e) {
                    e.printStackTrace();
                    statusCodeErrorCheck.onstatusCodeCheck(false);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(VolleyError error, int fromWhere) {
                statusCodeErrorCheck.onstatusCodeCheck(false);
            }

            @Override
            public void onStatusCodeError(int statusCode) {

            }
        });
    }

    public static Bitmap grayScaleImage(Bitmap src) {
        // constant factors
        final double GS_RED = 0.299;
        final double GS_GREEN = 0.587;
        final double GS_BLUE = 0.114;

        // create output bitmap
        Bitmap bmOut = Bitmap.createBitmap(src.getWidth(), src.getHeight(), src.getConfig());
        // pixel information
        int A, R, G, B;
        int pixel;

        // get image size
        int width = src.getWidth();
        int height = src.getHeight();

        // scan through every single pixel
        for (int x = 0; x < width; ++x) {
            for (int y = 0; y < height; ++y) {
                // get one pixel color
                pixel = src.getPixel(x, y);
                // retrieve color of all channels
                A = Color.alpha(pixel);
                R = Color.red(pixel);
                G = Color.green(pixel);
                B = Color.blue(pixel);
                // take conversion up to one single value
                R = G = B = (int) (GS_RED * R + GS_GREEN * G + GS_BLUE * B);
                // set new pixel color to output bitmap
                bmOut.setPixel(x, y, Color.argb(A, R, G, B));
            }
        }

        // return final image
        return bmOut;
    }


}
