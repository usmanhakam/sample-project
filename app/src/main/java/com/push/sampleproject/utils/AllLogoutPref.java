package com.push.sampleproject.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Haider on 1/18/2016.
 */
public class AllLogoutPref {

    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context _context;

    public static final String PREF_APP = "logout_pref";
    // All Shared Preferences Keys
    private static final String IS_LOGOUT = "IsLogout";

    // Constructor
    @SuppressLint("CommitPrefEdits")
    public AllLogoutPref(Context context, String pref_fname) {
        this._context = context;

        pref = _context.getSharedPreferences(pref_fname, Context.MODE_PRIVATE);
        editor = pref.edit();

    }

    public boolean getAllLogout() {
        return pref.getBoolean(IS_LOGOUT, false);
    }

    public void setAllLogout(boolean isLogout) {
        editor.putBoolean(IS_LOGOUT, isLogout);
        editor.commit();
    }
}
