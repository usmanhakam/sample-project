package com.push.sampleproject.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.StateListDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.barfee.mart.CategoriesList;
import com.barfee.mart.ChangeLocality;
import com.barfee.mart.DeliveryLocation;
import com.barfee.mart.MyApplication;
import com.barfee.mart.R;
import com.barfee.mart.interfaces.ResponseListenerForApiCall;
import com.barfee.mart.models.Customer;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class Globals {

    private static Globals instance = null;

    public Locale mLocale;
    public Activity mActivity;
    public Context mContext;
    public String mPackageName;
    public int mScreenWidth;
    public int mScreenHeight;

    public AppSession appSession;
    public AllLogoutPref allLogoutPref;
    public PromoCodePref promoCodePref;

    public String gcmToken;

    public String addressName;
    public double lati, longi;

    public Customer customer;

    public int fromWhere;
    public JSONObject withoutLoginJson;

    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    private Globals(Activity activity) {
        setContext(activity);
        mLocale = Locale.ENGLISH;
        mPackageName = mContext.getPackageName();
        appSession = new AppSession(mContext, AppSession.PREF_APP);
        allLogoutPref = new AllLogoutPref(mContext, AllLogoutPref.PREF_APP);
        promoCodePref = new PromoCodePref(mContext);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB_MR2) {
            Point size = new Point();
            mActivity.getWindowManager().getDefaultDisplay().getSize(size);
            mScreenWidth = size.x;
            mScreenHeight = size.y;
        } else {
            Display display = mActivity.getWindowManager().getDefaultDisplay();
            mScreenWidth = display.getWidth();
            mScreenHeight = display.getHeight();
        }

        gcmToken = "";
        addressName = "";
        lati = 0;
        longi = 0;
    }

    public static Globals getInstance(Activity activity) {
        if (instance == null) {
            instance = new Globals(activity);
        }
        instance.setContext(activity);
        return instance;
    }

    public static Globals getUsage() {
        return instance;
    }

    private void setContext(Activity activity) {
        mActivity = activity;
        mContext = activity;
    }


    public void closeKeyBoard() {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) mActivity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(mActivity.getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Typeface setFont(Context context, String fontName) {
        return Typeface.createFromAsset(context.getAssets(), fontName) != null ?
                Typeface.createFromAsset(context.getAssets(), fontName) : Typeface.DEFAULT;
    }

    public static void setSelector(Context context, View view, int clicked, int normal) {
        StateListDrawable statesSignIn = new StateListDrawable();
        statesSignIn.addState(new int[]{android.R.attr.state_pressed},
                context.getResources().getDrawable(clicked));
        statesSignIn.addState(new int[]{android.R.attr.state_focused},
                context.getResources().getDrawable(clicked));
        statesSignIn.addState(new int[]{android.R.attr.state_selected},
                context.getResources().getDrawable(clicked));
        statesSignIn.addState(new int[]{},
                context.getResources().getDrawable(normal));

        view.setBackgroundDrawable(statesSignIn);

    }

    public static void setSelector(Context context, View view, int clicked, int normal, int disabled) {
        StateListDrawable statesSignIn = new StateListDrawable();
        statesSignIn.addState(new int[]{android.R.attr.state_pressed},
                context.getResources().getDrawable(clicked));
        statesSignIn.addState(new int[]{android.R.attr.state_focused},
                context.getResources().getDrawable(clicked));
        statesSignIn.addState(new int[]{android.R.attr.state_selected},
                context.getResources().getDrawable(clicked));
        statesSignIn.addState(new int[]{-android.R.attr.state_enabled},
                context.getResources().getDrawable(disabled));
        statesSignIn.addState(new int[]{},
                context.getResources().getDrawable(normal));

        view.setBackgroundDrawable(statesSignIn);

    }

    public static void applyBorderToAView(Context context, View v, int strokeColor, int bgColor) {
        GradientDrawable gd = new GradientDrawable();
        gd.setColor(ContextCompat.getColor(context, bgColor)); // Changes this drawbale to use a single color instead of a gradient
        gd.setStroke(2, ContextCompat.getColor(context, strokeColor));
        v.setBackgroundDrawable(gd);
    }

    public static void setSelectorRounded(View view, int clicked, int normal) {
        GradientDrawable gdSimple = new GradientDrawable();
        gdSimple.setShape(GradientDrawable.RECTANGLE);
        gdSimple.setColor(normal); // Changes this drawbale to use a single color instead of a gradient
        gdSimple.setCornerRadius(5);
        GradientDrawable gdPress = new GradientDrawable();
        gdPress.setShape(GradientDrawable.RECTANGLE);
        gdPress.setColor(clicked); // Changes this drawbale to use a single color instead of a gradient
        gdPress.setCornerRadius(5);
        StateListDrawable statesSignIn = new StateListDrawable();
        statesSignIn.addState(new int[]{android.R.attr.state_pressed}, gdPress);
        statesSignIn.addState(new int[]{android.R.attr.state_focused}, gdPress);
        statesSignIn.addState(new int[]{android.R.attr.state_selected}, gdPress);
        statesSignIn.addState(new int[]{}, gdSimple);
        view.setBackgroundDrawable(statesSignIn);

    }

    public static ShapeDrawable getDefaultBackground(Context context) {
        int r = dipToPixels(context, 8);
        float[] outerR = new float[]{(float) r, (float) r, (float) r, (float) r, (float) r, (float) r, (float) r, (float) r};
        RoundRectShape rr = new RoundRectShape(outerR, (RectF) null, (float[]) null);
        ShapeDrawable drawable = new ShapeDrawable(rr);
        drawable.getPaint().setColor(context.getResources().getColor(R.color.colorPrimary));
        return drawable;
    }

    private static int dipToPixels(Context context, int dip) {
        Resources r = context.getResources();
        float px = TypedValue.applyDimension(1, (float) dip, r.getDisplayMetrics());
        return (int) px;
    }

    public void createVibration() {
//        Vibrator v = (Vibrator) mContext.getSystemService(Context.VIBRATOR_SERVICE);
        // Vibrate for 500 milliseconds
//        v.vibrate(50);
//        final MediaPlayer mp = new MediaPlayer();
//        if (mp.isPlaying())
//            mp.stop();
//        try {
//            mp.reset();
//            AssetFileDescriptor afd;
//            afd = mContext.getAssets().openFd("blop.mp3");
//            mp.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
//            mp.prepare();
//            mp.start();
//        } catch (IllegalStateException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }

    public void saveTokenToPrefs(String _token) {
        // Access Shared Preferences
        SharedPreferences pref = mContext.getSharedPreferences("FCMToken", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        // Save to SharedPreferences
        editor.putString("registration_id", _token);
        editor.commit();
    }

    public String getTokenFromPrefs() {
        SharedPreferences pref = mContext.getSharedPreferences("FCMToken", Context.MODE_PRIVATE);
        return pref.getString("registration_id", "");
    }

    private ProgressDialog progressDialog;

    public void apiCallForLocation(final Context context, final Location mLocal, final int fromWhereIndex, final boolean isAuto) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Verfiying locality...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("latitude", mLocal.getLatitude() + "");
        params.put("longitude", mLocal.getLongitude() + "");


        Constants.apiPostRequest(context, Request.Method.POST, Constants.URL_MAP_CHECK_FOR_COORDINATES, params, 0, new ResponseListenerForApiCall() {
            @Override
            public void onSuccess(JSONObject jsonObject, int fromWhere) {
                try {

                    Log.d("Api Call for Laocation", jsonObject.toString());
                    if (jsonObject != null) {
                        if (jsonObject.has("message")) {

                                withoutLoginJson =jsonObject;
                                getAddressFromLocation(context, mLocal, fromWhereIndex, isAuto);


                        } else {
                            String error = jsonObject.has("error") ? jsonObject.getString("error") : "No location";
                            Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
                            showChangeLocalityScreen(context);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(VolleyError error, int fromWhere) {
                try {
                    progressDialog.dismiss();
                    if (error instanceof AuthFailureError
                            || error instanceof ParseError) {
                        AlertDialogs.getInstance().showDialogOK(context, Constants.SERVER_HEADING, Constants.SERVER_DESC, null, false);
                        //TODO
                    } else if (error instanceof NetworkError
                            || error instanceof TimeoutError
                            || error instanceof NoConnectionError) {
                        AlertDialogs.getInstance().showDialogOK(context, Constants.INTERNET_HEADING, Constants.INTERNET_DESC, null, false);
                    } else
                        AlertDialogs.getInstance().showDialogOK(context, Constants.SERVER_HEADING, Constants.SERVER_DESC, null, false);
                } catch (WindowManager.BadTokenException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onStatusCodeError(int statusCode) {
                progressDialog.dismiss();
                AlertDialogs.getInstance().showDialogOK(context, Constants.SERVER_HEADING, Constants.SERVER_DESC, null, false);
            }
        });
    }

    public void showChangeLocalityScreen(Context context) {
        Intent intent = new Intent(context, ChangeLocality.class);
        context.startActivity(intent);
        ((Activity) context).finish();
    }

    public void getAddressFromLocation(Context context, Location mLocal, int fromWhere, boolean isAuto) {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(context, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(mLocal.getLatitude(), mLocal.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            if (addresses != null && addresses.size() > 0) {
                String fullAddress = addresses.get(0).getAddressLine(0) + " " +
                        addresses.get(0).getLocality() + " " +
                        addresses.get(0).getCountryName();
                if (fromWhere == Constants.ALREADY_LOGGED_IN || fromWhere == Constants.SIGN_IN ||
                        fromWhere == Constants.MAIN_ACT) {
                    apiCallForUpdate(context, fullAddress, mLocal, fromWhere, isAuto);
                } else if (fromWhere == Constants.WITHOUT_LOGIN) {
                    if (appSession.createVendorWithOutLogin(withoutLoginJson)) {
                        appSession.setAddress(fullAddress);
                        MyApplication.getInstance().trackEvent("Vendor",appSession.getVendorName(),"");
                     //   Log.d("Traaaa",appSession.getVendorName());
                       // Log.d("ssssssssssssssssss","Without Login>>>>>>"+withoutLoginJson.toString());
                        progressDialog.dismiss();
                        appSession.setCoordinates(mLocal.getLatitude() + "," + mLocal.getLongitude());
                        Intent i = new Intent(context, CategoriesList.class);
                        this.lati = mLocal.getLatitude();
                        this.longi = mLocal.getLongitude();
                        context.startActivity(i);
                        mActivity.finish();
                    }
                    }else if (fromWhere == Constants.SIGN_UP) {
                    Log.d("ssssssssssssssssss","SignUp");
                    proceedToActivity(context, fullAddress, mLocal, fromWhere, isAuto);

                    // p
                } else
                    proceedToActivity(context, fullAddress, mLocal, fromWhere, isAuto);
            } else {
                progressDialog.dismiss();
                Log.d("looooocation", mLocal.getLongitude() + "<><>" + mLocal.getLatitude());
                Toast.makeText(context, "Unable to fetch location", Toast.LENGTH_SHORT).show();
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.d("looooocation", mLocal.getLongitude() + "<><>" + mLocal.getLatitude());
            progressDialog.dismiss();
            Toast.makeText(context, "Unable to fetch location Exception", Toast.LENGTH_SHORT).show();
        }
    }

    public void apiCallForUpdate(final Context context, final String addressName, final Location mLocation, final int fromWhereVar,
                                 final boolean isAuto) {

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Updating profile ...");
        progressDialog.setCancelable(false);

        progressDialog.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("address", addressName);
        params.put("location_coordinates", mLocation.getLatitude() + "," + mLocation.getLongitude());
        String url = Constants.URL_UPDATE;
        if (fromWhereVar == Constants.SIGN_IN) {
            url = url + customer.getUserToken();
            params.put("email", customer.getEmail());
            params.put("name", customer.getName());
            params.put("address_line_2", customer.getAddress2() == null ? "" : customer.getAddress2());
        }
        if (fromWhereVar == Constants.WITHOUT_LOGIN) {

        } else {
            url = url + appSession.getToken();
            params.put("email", appSession.getEmail());
            params.put("name", appSession.getName());
            params.put("address_line_2", appSession.getAddress2() == null ? "" : appSession.getAddress2());
        }
        Constants.apiPostRequest(context, Request.Method.POST, url, params, 0, new ResponseListenerForApiCall() {
            @Override
            public void onError(VolleyError error, int fromWhere) {
                progressDialog.dismiss();
                try {
                    if (error instanceof AuthFailureError
                            || error instanceof ParseError) {
                        AlertDialogs.getInstance().showDialogOK(context, Constants.SERVER_HEADING, Constants.SERVER_DESC, null, false);
                        //TODO
                    } else if (error instanceof NetworkError
                            || error instanceof TimeoutError
                            || error instanceof NoConnectionError) {
                        AlertDialogs.getInstance().showDialogOK(context, Constants.INTERNET_HEADING, Constants.INTERNET_DESC, null, false);
                    } else
                        AlertDialogs.getInstance().showDialogOK(context, Constants.SERVER_HEADING, Constants.SERVER_DESC, null, false);
                } catch (WindowManager.BadTokenException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onSuccess(JSONObject jsonObject, int fromWhere) {
                progressDialog.dismiss();
                if (jsonObject != null) {
                    try {
                        if (jsonObject.has("error")) {
                            if (jsonObject.getJSONObject("error").has("phone_number")) {
                                AlertDialogs.getInstance().showDialogOK(context, "Already Taken", "Phone number has already been registered.", null, false);
                            }
                        } else {
                            Customer customer = new Gson().fromJson(jsonObject.getJSONObject("customer").toString(),
                                    Customer.class);
                            customer.setUserToken(jsonObject.has("token") ? jsonObject.getString("token") : "");
                            Globals.getUsage().customer = new Customer(customer);
                            if (fromWhereVar == Constants.SIGN_IN)
                                appSession.createLoginSession(jsonObject, customer.getPassword());


                            else {
                                appSession.updateDataAfterLocationChange(customer);
                            }
                            proceedToActivity(context, addressName, mLocation, fromWhereVar, isAuto);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onStatusCodeError(int statusCode) {
                progressDialog.dismiss();
            }
        });
    }

    public void proceedToActivity(Context context, String addressName, Location mLocation, int fromWhere,
                                  boolean isAuto) {
        Intent intent = new Intent();
        saveLocation(mLocation);
        MyApplication.getInstance().trackEvent("Vendor",appSession.getVendorName(),"");
        Log.d("Traaaa",appSession.getVendorName());
        if (fromWhere == Constants.SIGN_UP) {
            this.addressName = addressName;
            this.lati = mLocation.getLatitude();
            this.longi = mLocation.getLongitude();


        } else if (fromWhere == Constants.SIGN_IN || fromWhere == Constants.ALREADY_LOGGED_IN) {
            intent = new Intent(context, CategoriesList.class);
            intent.putExtra("address", addressName);
            intent.putExtra("latitude", mLocation.getLatitude());
            intent.putExtra("longitude", mLocation.getLongitude());

        } else if (fromWhere == Constants.EDIT_PROFILE) {
            this.addressName = addressName;
            this.lati = mLocation.getLatitude();
            this.longi = mLocation.getLongitude();
        } else if (fromWhere == Constants.MAIN_ACT) {
            this.addressName = addressName;
            this.lati = mLocation.getLatitude();
            this.longi = mLocation.getLongitude();
        } else if (fromWhere == Constants.WITHOUT_LOGIN) {
            this.addressName = addressName;
            this.lati = mLocation.getLatitude();
            this.longi = mLocation.getLongitude();
        }
        if (fromWhere != Constants.EDIT_PROFILE
                && fromWhere != Constants.SIGN_UP
                && fromWhere != Constants.MAIN_ACT)
            context.startActivity(intent);

        if (DeliveryLocation.deliveryLocation != null)
            DeliveryLocation.deliveryLocation.finish();
        ((Activity) context).finish();
    }


  /*  public void trackingEventsInTuneDev(String string){
        TuneEvent tuneEvent = new TuneEvent(string);
        Tune.getInstance().measureEvent(tuneEvent);
    }*/


    public void saveLocation(Location mLocation){

        appSession.setCoordinates(mLocation.getLatitude() + "," + mLocation.getLongitude());
        Log.d("savinglocations    ",mLocation.getLatitude() + "," + mLocation.getLongitude());
    }
}