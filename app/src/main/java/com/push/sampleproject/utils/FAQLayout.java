package com.push.sampleproject.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.barfee.mart.R;

import net.cachapa.expandablelayout.ExpandableLinearLayout;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Haider on 14/08/2016.
 */
public class FAQLayout extends LinearLayout {

    @Bind(R.id.tv_heading)
    TextView textViewHeading;
    @Bind(R.id.tv_desc)
    TextView textViewDesc;
    @Bind(R.id.expandable_layout)
    ExpandableLinearLayout expandableLinearLayout;

    private String heading;
    private String description;

    public FAQLayout(Context context) {
        this(context, null);
    }

    public FAQLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.custom_faq_layout, this, true);
        ButterKnife.bind(this, v);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.FAQLayout);
        final int N = a.getIndexCount();
        for (int i = 0; i < N; ++i) {
            int attr = a.getIndex(i);
            switch (attr) {
                case R.styleable.FAQLayout_desc:
                    setDescription(a.getString(attr));
                    break;
                case R.styleable.FAQLayout_heading:
                    setHeading(a.getString(attr));
                    break;

            }
        }
        a.recycle();

        textViewDesc.setTypeface(Globals.getUsage().setFont(Globals.getUsage().mContext, Constants.robotolight));
        textViewHeading.setTypeface(Globals.getUsage().setFont(Globals.getUsage().mContext, Constants.robotolight));

    }

    @OnClick(R.id.ll_heading)
    public void onClick(View v) {
        expandableLinearLayout.toggle(true);
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        textViewHeading.setText(heading);
        this.heading = heading;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        textViewDesc.setText(description);
        this.description = description;
    }
}