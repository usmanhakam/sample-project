package com.push.sampleproject.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.barfee.mart.models.Customer;

import org.json.JSONObject;

import io.intercom.android.sdk.Intercom;
import io.intercom.android.sdk.identity.Registration;

/**
 * Created by Haider on 1/18/2016.
 */
public class AppSession {

    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context _context;

    public static final String PREF_APP = "Berfeemart_pref";
    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";

    public static final String CUSTOMER_KEY = "userkey";
    public static final String CUSTOMER_TOKEN = "usertoken";
    public static final String CUSTOMER_ADDRESS = "address";
    public static final String CUSTOMER_ADDRESS_2 = "address2";
    public static final String CUSTOMER_PASSWORD = "password";
    public static final String CUSTOMER_EMAIL = "email";
    public static final String CUSTOMER_PHONE = "phone";
    public static final String CUSTOMER_COORDINATE = "coordinates";
    public static final String VENDOR_NAME = "vendor_name";
    public static final String VENDOR_EMAIL = "vendor_email";
    public static final String VENDOR_MINUTES = "vendor_minutes";
    public static final String VENDOR_ADDRESS = "vendor_address";
    public static final String VENDOR_IMAGE = "vendor_image";
    public static final String VENDOR_ID = "vendor_id";
    public static final String HAS_VENDOR = "has_vendor";

    // Constructor
    @SuppressLint("CommitPrefEdits")
    public AppSession(Context context, String pref_fname) {
        this._context = context;

        pref = _context.getSharedPreferences(pref_fname, Context.MODE_PRIVATE);
        editor = pref.edit();

    }

    /**
     * Create login session
     */
    public void createLoginSession(String email, String name, String phone, String address, String address2, String password, String token, String coordinates) {
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);

        editor.putString(CUSTOMER_KEY, name);
        editor.putString(CUSTOMER_PHONE, phone);
        editor.putString(CUSTOMER_ADDRESS, address);
        editor.putString(CUSTOMER_ADDRESS_2, address2);
        editor.putString(CUSTOMER_PASSWORD, password);
        editor.putString(CUSTOMER_TOKEN, token);
        editor.putString(CUSTOMER_EMAIL, email);
        editor.putString(CUSTOMER_COORDINATE, coordinates);
        Intercom.client().registerIdentifiedUser(Registration.create().withUserId(phone));
        // commit changes
        editor.commit();
    }

    /**
     * Create login session
     */
    public boolean createLoginSession(JSONObject jsonObject, String password) {
        // Storing login value as TRUE
        try {
//            "vendor": {
//                "id": 2,
//                        "name": "albarkat",
//                        "email": "albarkat@grocerapps.com",
//                        "minutes": "50",
    //                        "address": "DHA benzir inter chagne road",
//                        "image": "barkat.png",
//                        "created_at": "2016-11-21 19:00:00",
//                        "updated_at": "2016-12-30 15:49:54"
//            }
            if (jsonObject.has("customer")) {
                JSONObject obj = jsonObject.getJSONObject("customer");
                setToken(jsonObject.has("token") ? jsonObject.getString("token") : "");
                setName(obj.has("name") ? obj.getString("name") : "");
                setPhone(obj.has("phone_number") ? obj.getString("phone_number") : "");
                setAddress(obj.has("address") ? obj.getString("address") : "");
                setAddress2(obj.has("address_2") ? obj.getString("address_2") : "");
                setEmail(obj.has("email") ? obj.getString("email") : "");
                setCoordinates(obj.has("location_coordinates") ? obj.getString("location_coordinates") : "");
                setPassword(password);
                if (obj.has("vendor")) {
                    JSONObject objVendor = obj.getJSONObject("vendor");
                    if (objVendor != null) {
                        setVendorAddress(objVendor.getString("address"));
                        setVendorEmail(objVendor.getString("email"));
                        setVendorName(objVendor.getString("name"));
                        setVendorId(objVendor.getInt("id"));
                        setVendorMinutes(objVendor.getInt("minutes"));
                        setVendorImage(objVendor.getString("image"));


                     /*   setVendorAddress(jsonObject.has("address") ? jsonObject.getString("address") : "");
                        setVendorEmail(jsonObject.has("email") ? jsonObject.getString("email") : "");
                        setVendorName(jsonObject.has("name") ? jsonObject.getString("name") : "");
                        setVendorId(jsonObject.has("id") ? jsonObject.getInt("id") : 0);
                        setVendorMinutes(jsonObject.has("minutes") ? jsonObject.getInt("minutes") : 0);
                        setVendorImage(jsonObject.has("image") ? jsonObject.getString("image") : "");*/


                    }
                }
                editor.putBoolean(IS_LOGIN, true);
              //  editor.putBoolean(HAS_VENDOR, false);
                editor.commit();
                Intercom.client().registerIdentifiedUser(Registration.create().withUserId(obj.getString("phone_number")));

                return true;
            } else
                return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    public void updateDataAfterLocationChange(Customer customer) {
        setVendorAddress(customer.getVendor().getAddress());
        setVendorEmail(customer.getVendor().getEmail());
        setVendorName(customer.getVendor().getName());
        setVendorId(customer.getVendor().getId());
        try {
            setVendorMinutes(Integer.parseInt(customer.getVendor().getMinutes()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        setVendorImage(customer.getVendor().getImage());
        setAddress(customer.getAddress());
        setCoordinates(customer.getLocationCoordinates());

    }

    public String getToken() {
        return pref.getString(CUSTOMER_TOKEN, null);
    }

    public void setToken(String Name) {
        editor.putString(CUSTOMER_TOKEN, Name);
        editor.commit();
    }

    public String getName() {
        return pref.getString(CUSTOMER_KEY, null);
    }

    public void setName(String Name) {
        editor.putString(CUSTOMER_KEY, Name);
        editor.commit();
    }

    public String getPhone() {
        return pref.getString(CUSTOMER_PHONE, null);
    }

    public void setPhone(String phone) {
        editor.putString(CUSTOMER_PHONE, phone);
        editor.commit();
    }

    public String getAddress() {
        return pref.getString(CUSTOMER_ADDRESS, null);
    }

    public void setAddress(String Address) {
        editor.putString(CUSTOMER_ADDRESS, Address);
        editor.commit();
    }

    public String getAddress2() {
        return pref.getString(CUSTOMER_ADDRESS_2, null);
    }

    public void setAddress2(String Address) {
        editor.putString(CUSTOMER_ADDRESS_2, Address);
        editor.commit();
    }


    public void setPassword(String Password) {
        editor.putString(CUSTOMER_PASSWORD, Password);
        editor.commit();
    }

    public String getPassword() {
        return pref.getString(CUSTOMER_PASSWORD, null);
    }

    public void setEmail(String Email) {
        editor.putString(CUSTOMER_EMAIL, Email);
        editor.commit();
    }

    public String getEmail() {
        return pref.getString(CUSTOMER_EMAIL, null);
    }


    public void setCoordinates(String coordinates) {
        editor.putString(CUSTOMER_COORDINATE, coordinates);
        editor.commit();
    }

    public String getCoordinates() {
        String coordinates = pref.getString(CUSTOMER_COORDINATE, null);
        if (coordinates == null)
            return coordinates;
        if (coordinates.equals("") || coordinates.equals("null"))
            coordinates = null;
        return coordinates;
    }


    public String getVendorName() {
        return pref.getString(VENDOR_NAME, null);
    }

    public void setVendorName(String vendorName) {
        editor.putString(VENDOR_NAME, vendorName);
        editor.commit();
    }

    public String getVendorEmail() {
        return pref.getString(VENDOR_EMAIL, null);
    }

    public void setVendorEmail(String vendorEmail) {
        editor.putString(VENDOR_EMAIL, vendorEmail);
        editor.commit();
    }

    public int getVendorMinutes() {
        return pref.getInt(VENDOR_MINUTES, 0);
    }

    public void setVendorMinutes(int vendorMinutes) {
        editor.putInt(VENDOR_MINUTES, vendorMinutes);
        editor.commit();
    }

    public String getVendorAddress() {
        return pref.getString(VENDOR_ADDRESS, null);
    }

    public void setVendorAddress(String vendorAddress) {
        editor.putString(VENDOR_ADDRESS, vendorAddress);
        editor.commit();
    }

    public String getVendorImage() {
        return pref.getString(VENDOR_IMAGE, null);
    }

    public void setVendorImage(String vendorImage) {
        editor.putString(VENDOR_IMAGE, vendorImage);
        editor.commit();
    }

    public int getVendorId() {
        return pref.getInt(VENDOR_ID, -1);
    }

    public void setVendorId(int vendorId) {
        editor.putInt(VENDOR_ID, vendorId);
        editor.commit();
    }

    /**
     * Clear session details
     */
    public void logoutUser() {
        // Clearing all data from Shared Preferences

        editor.clear();
        editor.commit();
        Intercom.client().reset();

    }

    /**
     * Quick check for login
     **/
    // Get Login State
    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub

        return "name : " + getName() +
                " phone : " + getPhone() +
                " address : " + getAddress() +
                " password : " + getPassword();

    }


    public boolean createVendorWithOutLogin(JSONObject jsonObject){
        try {

            if (jsonObject.has("vendor")) {
                    JSONObject objVendor = jsonObject.getJSONObject("vendor");

                    if (objVendor != null) {
                        Log.d("hasvendor",">>>>>>"+"innnnnnn"+"<<<");
                       // setVendorAddress(objVendor.has("address") ? objVendor.getString("address") : "");
                        setVendorEmail(objVendor.has("email") ? objVendor.getString("email") : "");
                        setVendorName(objVendor.has("name") ? objVendor.getString("name") : "");
                        setVendorId(objVendor.has("id") ? objVendor.getInt("id") : 0);
                        setVendorMinutes(objVendor.has("minutes") ? objVendor.getInt("minutes") : 0);
                        setVendorImage(objVendor.has("image") ? objVendor.getString("image") : "");

                    }

                editor.putBoolean(HAS_VENDOR, true);
                editor.commit();
                return true;
            } else
                return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }



    public boolean hasVendor() {
        return pref.getBoolean(HAS_VENDOR, false);
    }


}
